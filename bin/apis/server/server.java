/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apis.server;


import java.util.Date;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
// import java.net.*;
import java.awt.event.*;
import java.io.*;
import static java.nio.file.StandardOpenOption.WRITE;
import data.user.files.fileManager;
import static java.nio.file.StandardOpenOption.DELETE_ON_CLOSE;
import java.nio.file.attribute.FileAttribute;

/**
 *
 * @author hexakova
 */
// this is the config file that configures the server and all its connections including input and output

public class server{
    Date today = new Date();
    public void startLogging() {
        Charset charset = Charset.forName("US-ASCII");
        Path logPath = Paths.get("log.txt");
        String startConfirm = "###@@$$$ Logger started successfully at " + today; 
        try {
            BufferedWriter logger = Files.newBufferedWriter(logPath, charset, WRITE);
            logger.write(startConfirm);
            System.out.println(startConfirm);
           
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    public void dbInit() {
        link dbInit = new link();
        dbInit.dblink();
        
    }
    public void stopScript() throws InterruptedException{
        this.wait(999999999);
    }
    public void startServing() {
        System.out.println("###@@$$$ Getting config files ---------------------");
        Path configs = Paths.get("bin/data/user/files");
        System.out.println("###@@$$$ Config files found starting server -------");
        System.out.println("###@@$$$ Server started successfully at " + today);
        System.out.println("Checking for number of connected users ------------");
        
//          for (int users = 0; users < 194550; users++) {
//             System.out.println("Number of connected users =" + users);
//           }
        System.out.println("Oops I guess thats all for now ");
        System.out.println("Waiting for further instructions");
        // Entering the tomcat server starter directory
        System.out.println("cd C:/xampp/tomcat");
        // Starting the server and routing output to log file
        System.out.println("catalina_start.bat > log.txt");     
    }
    public void fileManagerInit() throws Exception{
        System.out.println("Searching for main directories---------------------");
        Path mainDir = Paths.get("bin/data/user/files");
//        if (mainDir.toString() == "files"){
            System.out.println("Found main directories. Converting to real paths");
        Path mainDir2 = mainDir.toRealPath(LinkOption.NOFOLLOW_LINKS);
        
        System.out.println("Main directories located at " + mainDir2.toString());
        System.out.println("Initializing file creation scripts");
//        }else{
//            System.out.println("Main Directories not found exiting script, stopping server.........");
//            stopScript();
//        }
        
        System.out.println("Creating test file to check validity of create file scripts");
        Path testFile;
        Path nimp = Paths.get("bin/data/user/files/testfile.txt");
        testFile = Files.createFile(nimp);
        System.out.println("Test file successfully created. File scripts valid. Test file = '" + testFile + "'");
        
        System.out.println("Deleting test file");
        Path temp = Paths.get(testFile.toString());
        Files.delete(temp);
        System.out.println("Test file deleted");
        
        fileManager test = new fileManager();
        Path movie = Paths.get("C:\\Users\\hexakova\\Documents\\Lynda Java\\86005_03_01_SC11_HelloWorld-mov.mov");
        test.copyFile(movie);
        test.watchDir(mainDir);
               
        
    }
    public static void main(String[] args) throws Exception{
        server berk = new server();
        berk.startLogging();
        berk.startServing();
        berk.dbInit();
        berk.fileManagerInit();
            
        
        
        
    }
    
}
